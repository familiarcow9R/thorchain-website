export interface AppsAndServicesItem {
	title: string;
	url: string;
	logo: string;
	description: string;
	tags: string[];
}
