import { getStats } from '$lib/services/yield';
import axios from 'axios';
import { totalSupply } from './constants';
import { convertToPercentage, formatNumber, formatNumberTwoDecimal } from './helpers';
import { getRunePrice } from './rune';

const calculateTotalValueLocked = (
	totalValuePooled: number,
	totalActiveRuneBond: number,
	totalStandByRuneBond: number,
	runePriceUSD: number,
): number => {
	return ((totalValuePooled + totalActiveRuneBond + totalStandByRuneBond) / 1e8) * runePriceUSD;
};

const calculateDailyTradingVolume = (totalVolume: number, runePriceUSD: number): number => {
	return totalVolume / 1e8 / 7 * runePriceUSD;
};

const calculateTotalVolumeUSD = (totalVolume: number, runePriceUSD: number): number => {
	return (totalVolume / 1e8) * runePriceUSD;
};

const calculateTotalPoolEarnings = (liquidityEarnings: number, runePriceUSD: number): number => {
	return (liquidityEarnings / 1e8) * runePriceUSD;
};

const calculateTotalValuePooled = (totalPooledRune: number, runePriceUSD: number): number => {
	return (totalPooledRune / 1e8) * runePriceUSD;
};

const calculateTotalLiquidity = (totalPooledRune: number, runePriceUSD: number): number => {
	return (totalPooledRune / 1e8) * runePriceUSD * 2;
};

const calculateTotalBondedRune = (totalActiveBond: number, runePriceUSD: number): number => {
	return (totalActiveBond / 1e8) * runePriceUSD;
};

const calculateTotalPooledRune = (totalPooledRune: number, runePriceUSD: number): number => {
	return (totalPooledRune / 1e8) * runePriceUSD;
};

export interface Interval {
	endTime: string;
	runePriceUSD: string;
	startTime: string;
	totalValuePooled: string;
}

export type Meta = Interval;

export interface HistoryResponse {
	intervals: Interval[];
	meta: Meta;
}

export const getHistory = async () => {
	const { data } = await axios.get<HistoryResponse>(
		'https://midgard.ninerealms.com/v2/history/tvl',
	);

	return data;
};

export interface BlockRewards {
	blockReward: string;
	bondReward: string;
	poolReward: string;
}

export interface BondMetrics {
	averageActiveBond: string;
	averageStandbyBond: string;
	maximumActiveBond: string;
	maximumStandbyBond: string;
	medianActiveBond: string;
	medianStandbyBond: string;
	minimumActiveBond: string;
	minimumStandbyBond: string;
	totalActiveBond: string;
	totalStandbyBond: string;
}

export interface NetworksResponse {
	activeBonds: string[];
	activeNodeCount: string;
	blockRewards: BlockRewards;
	bondMetrics: BondMetrics;
	bondingAPY: string;
	liquidityAPY: string;
	nextChurnHeight: string;
	poolActivationCountdown: string;
	poolShareFactor: string;
	standbyBonds: string[];
	standbyNodeCount: string;
	totalPooledRune: string;
	totalReserve: string;
}

export const getNetworks = async () => {
	try {
		const { data } = await axios.get<NetworksResponse>('https://midgard.ninerealms.com/v2/network');

		return data;
	} catch (error) {
		console.log(error);
	}
};

export const getTotalValueLocked = async (): Promise<[number, number]> => {
    const networks = await getNetworks();
    const history = await getHistory();

    const totalValuePooled = +history.meta.totalValuePooled;
    const runePriceUSD = +history.meta.runePriceUSD;
    const totalActiveRuneBond = +networks.bondMetrics.totalActiveBond;
    const totalStandByRuneBond = +networks.bondMetrics.totalStandbyBond;

    const totalBondUSD = (totalActiveRuneBond + totalStandByRuneBond) * runePriceUSD / 1e8;

    const totalValueLocked = calculateTotalValueLocked(
        totalValuePooled,
        totalActiveRuneBond,
        totalStandByRuneBond,
        runePriceUSD,
    );

    return [totalValueLocked, totalBondUSD];
};

export const getActiveNodeCount = async (): Promise<{
	activeNodeCount: string;
	bondingAPY: string;
}> => {
	try {
		const networks = await getNetworks();
		const activeNodeCount = +networks.activeNodeCount;
		const bondingAPY = +networks.bondingAPY;
		return {
			activeNodeCount: activeNodeCount.toString(),
			bondingAPY: bondingAPY.toString(),
		};
	} catch (error) {
		console.error(error);
	}
};

export const getSaversAPYs = async (): Promise<{
    ethSaversAPY: number;
    btcSaversAPY: number;
}> => {
    try {
        const response = await axios.get('https://midgard.ninerealms.com/v2/pools');

        const ethPool = response.data.find((pool: any) => pool.asset === 'ETH.ETH');
        const btcPool = response.data.find((pool: any) => pool.asset === 'BTC.BTC');

        if (!ethPool || !btcPool) {
            throw new Error('ETH.ETH or BTC.BTC pool not found.');
        }

        const ethSaversAPY = parseFloat(ethPool.saversAPR);
        const btcSaversAPY = parseFloat(btcPool.saversAPR);

        return {
            ethSaversAPY: ethSaversAPY,
            btcSaversAPY: btcSaversAPY,
        };
    } catch (error) {
        console.error(error);
    }
};

export const getTotalAddresses = async (): Promise<number> => {
	try {
		const response = await axios.get('https://api.flipsidecrypto.com/api/v2/queries/9b0a9364-6adc-4f6e-bfe4-12d3d116e901/data/latest');
		const totalAddresses = response.data[0].NEW_ADDRESSES_CUMULATIVE;

		return totalAddresses;
	} catch (error) {
		console.error(error);
	//	return 0;
	}
};


export const getDailyTradingVolume = async (): Promise<number> => {
	try {
		const response = await axios.get(
			'https://midgard.ninerealms.com/v2/history/swaps?interval=day&count=7',
		);
		const totalVolume = +response.data.meta.totalVolume;
		const runePriceUSD = +response.data.meta.runePriceUSD;

		return calculateDailyTradingVolume(totalVolume, runePriceUSD);
	} catch (error) {
		console.error(error);
	}
};

export const getTotalVolumeUSD = async (): Promise<{totalVolumeUSD: number, totalCount: number}> => {
	try {
		const response = await axios.get('https://midgard.ninerealms.com/v2/history/swaps');
		const totalVolume = +response.data.meta.totalVolume;
		const runePriceUSD = +response.data.meta.runePriceUSD;
		const totalCount = +response.data.meta.totalCount;

		return {
			totalVolumeUSD: calculateTotalVolumeUSD(totalVolume, runePriceUSD),
			totalCount
		};
	} catch (error) {
		console.log(error);
	}
};


export const getTotalPoolEarnings = async () => {
	try {
		const response = await axios.get(
			'https://midgard.ninerealms.com/v2/history/earnings',
		);
		const liquidityEarnings = +response.data.meta.liquidityEarnings;
		const runePriceUSD = +response.data.meta.runePriceUSD;

		return calculateTotalPoolEarnings(liquidityEarnings, runePriceUSD);
	} catch (error) {
		console.error(error);
	}
};

export const getLiquidity = async (): Promise<{
	liquidityAPY: number;
	totalLiquidity: number;
}> => {
	try {
		const networks = await getNetworks();
		const stats = await getStats();
		const totalPooledRune = +networks.totalPooledRune;
		const liquidityAPY = +networks.liquidityAPY;

		const totalLiquidity = calculateTotalLiquidity(totalPooledRune, Number(stats.runePriceUSD));

		return {
			liquidityAPY,
			totalLiquidity,
		};
	} catch (error) {
		console.error(error);
	}
};

export const getMarketCapAndCirculatingSupply = async (): Promise<{
	marketCap: string;
	circulatingSupply: string;
}> => {
	try {
		const response = await axios.get('https://api.coingecko.com/api/v3/coins/thorchain');
		return {
			marketCap: response.data.market_data.market_cap.usd,
			circulatingSupply: response.data.market_data.circulating_supply,
		};
	} catch (error) {
		console.log(error);
	}
};

export const getTotalPooledAndBondedRunes = async () => {
	try {
		const networks = await getNetworks();
		const runePriceUSD = await getRunePrice();

		const totalPooledRune = calculateTotalPooledRune(+networks.totalPooledRune, runePriceUSD);
		const totalBondedRune = calculateTotalBondedRune(
			+networks.bondMetrics.totalActiveBond,
			runePriceUSD,
		);

		return {
			totalPooledRune,
			totalBondedRune,
			runePriceUSD,
		};
	} catch (error) {
		console.error(error);
	}
};

export interface YieldMetrics {
	averageAPY: string;
	totalValuePooled: string;
}

export const getYieldMetrics = async (): Promise<YieldMetrics> => {
	const networks = await getNetworks();
	const stats = await getStats();

	const totalPooledRune = Number(networks.totalPooledRune);

	const totalValuePooled = calculateTotalValuePooled(totalPooledRune, Number(stats.runePriceUSD));

	return {
		totalValuePooled: `$${formatNumber(totalValuePooled)}`,
		averageAPY: `${convertToPercentage(networks.liquidityAPY, 1)}%`,
	};
};

export interface ChainMetrics {
	totalCount: any;
	totalPoolEarnings: string;
	totalVolumeUSD: string;
	totalValueLocked: string;
	dailyTradingVolume: string;
	totalLiquidity: string;
	liquidityAPY: string;
	activeNodeCount: string;
	bondingAPY: string;
	ethSaversAPY: string;
	btcSaversAPY: string;
	totalAddresses: string;
	totalBondUSD: string;
}

export const getChainMetrics = async (): Promise<ChainMetrics> => {
	const totalPoolEarnings = await getTotalPoolEarnings();
	const { totalVolumeUSD, totalCount }  = await getTotalVolumeUSD();
	const [totalValueLocked, totalBondUSD] = await getTotalValueLocked();
	const dailyTradingVolume = await getDailyTradingVolume();
	const { liquidityAPY, totalLiquidity } = await getLiquidity();
	const { activeNodeCount, bondingAPY } = await getActiveNodeCount();
	const { ethSaversAPY, btcSaversAPY } = await getSaversAPYs();
	const totalAddresses = await getTotalAddresses();



	return {
		totalPoolEarnings: `$${formatNumber(totalPoolEarnings)}`,
		totalVolumeUSD: `$${formatNumber(totalVolumeUSD)}`,
		totalValueLocked: `$${formatNumber(totalValueLocked)}`,
		dailyTradingVolume: `$${formatNumber(dailyTradingVolume)}`,
		totalLiquidity: `$${formatNumber(totalLiquidity)}`,
		liquidityAPY: `${convertToPercentage(liquidityAPY, 1)}%`,
		activeNodeCount: `${activeNodeCount}`,
		bondingAPY: `${convertToPercentage(bondingAPY, 1)}%`,
		ethSaversAPY: `${convertToPercentage(ethSaversAPY, 1)}%`,
        btcSaversAPY: `${convertToPercentage(btcSaversAPY, 1)}%`,
		totalCount: `${formatNumberTwoDecimal(totalCount)}`,
		totalAddresses: `${formatNumber(totalAddresses)}`,
        totalBondUSD: `$${formatNumber(totalBondUSD)}`,
	};
};

export interface RuneMetrics {
	runePriceUSD: string;
	totalPooledRune: string;
	totalBondedRune: string;
	marketCap: string;
	totalSupply: string;
	circulatingSupply: string;
}

export const getRuneMetrics = async (): Promise<RuneMetrics> => {
	const runePriceUSD = await getRunePrice();
	const { totalPooledRune, totalBondedRune } = await getTotalPooledAndBondedRunes();
	const { marketCap, circulatingSupply } = await getMarketCapAndCirculatingSupply();

	return {
		runePriceUSD: `$${formatNumber(runePriceUSD)}`,
		totalPooledRune: `$${formatNumber(totalPooledRune)}`,
		totalBondedRune: `$${formatNumber(totalBondedRune)}`,
		marketCap: `$${formatNumber(marketCap)}`,
		circulatingSupply: `${formatNumber(circulatingSupply)}`,
		totalSupply: formatNumber(totalSupply),
	};
};
