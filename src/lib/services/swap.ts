// pokemon.js
// Implementations for all the calls for the pokemon endpoints.
import { getHistory } from '$lib/services/metrics';
import axios from 'axios';
import type { StringNullableChain } from 'lodash';
import { assets } from './constants';
import { convertToCurrency, convertToPercentage, formatNumber } from './helpers';
import type { Asset } from './types';

export interface SwapData {
	name: string;
	chain: string;
	priceUSD: string;
	volume24h: string;
	saversDepth: string;
	saversAPR: string;
	swapURL: string;
	color: string;
	icon: any;
}

export const getAssetData = async (
	asset: Asset,
): Promise<{
	priceUSD: number;
	volume24h: number;
	saversDepth: number;
	saversAPR: number;
}> => {
	try {
		const response = await axios.get(`https://midgard.ninerealms.com/v2/pool/${asset}`);
		return {
			priceUSD: +response.data.assetPriceUSD,
			volume24h: +response.data.volume24h,
			saversDepth: +response.data.saverDepth,
			saversAPR: +response.data.saversAPR,
		};
	} catch (error) {
		console.error(error);
	}
};

export interface Pool {
	name: string;
	priceUSD: string;
	asset: string;
	assetDepth: string;
	assetPrice: string;
	assetPriceUSD: string;
	liquidityUnits: string;
	poolAPY: string;
	saversDepth: string;
	saversAPR: string;
	runeDepth: string;
	status: string;
	synthSupply: string;
	synthUnits: string;
	units: string;
	volume24h: string;
}

/* Since Savers is new and is not 180d old yet, 180d savers data cannot be displayed. Use legacy code once savers is over 180d old  for all displayed pools*/
/* This will display the default (30d) APR */

export const getPoolData = async (): Promise<Pool[]> => {
	const { data: poolsData } = await axios.get('https://midgard.ninerealms.com/v2/pools?period=180d');
	const { data: detailsData } = await axios.get('https://midgard.ninerealms.com/v2/pools');
  
	const poolData = poolsData.map((pool) => {
	  const details = detailsData.find((detail) => detail.asset === pool.asset);
  
	  return {
		...pool,
		name: pool.asset.split('-')[0],
		priceUSD: convertToCurrency(pool.assetPriceUSD),
		poolAPY: convertToPercentage(pool.annualPercentageRate, 1).toString(),
		saversDepth: (Math.round((pool.saversDepth / 1e8 ))),
		saversAPR: details ? convertToPercentage(details.saversAPR, 1).toString() : '0',
	  };
	});
  
	return poolData;
  };
  
  /* ORIGINAL CODE - replace to make saversAPR pull from 180d instead/*
  export const getPoolData = async (): Promise<Pool[]> => {
	const { data } = await axios.get('https://midgard.ninerealms.com/v2/pools');

	const poolData = data.map((data) => ({
		...data,
		name: data.asset.split('-')[0],
		priceUSD: convertToCurrency(data.assetPriceUSD),
		poolAPY: convertToPercentage(data.annualPercentageRate, 1).toString(),
		saversDepth: (Math.round((data.saversDepth / 1e8 ))),
		saversAPR: convertToPercentage(data.saversAPR, 1).toString(),
	}));

	return poolData;
}; 
*/

export const getSwapTableData = async (): Promise<SwapData[]> => {
	const tableData: SwapData[] = [];

	const poolData = await getPoolData();
	const history = await getHistory();

	for (const asset of assets) {
		const assetData = poolData.find((data) => data.asset === asset.assetCode);

		const volume24h = (Number(assetData.volume24h) / 1e8) * Number(history.meta.runePriceUSD);

		tableData.push({
			name: asset.name,
			chain: asset.chain,
			priceUSD: convertToCurrency(Number(assetData.assetPriceUSD)),
			volume24h: `$${formatNumber(volume24h)}`,
	//		saversDepth: `$${formatNumber(saversDepth)}`,
			swapURL: asset.swapURL,
			color: asset.color,
			icon: asset.icon,
		});
	}

	return tableData;
};

export interface MarketData {
	id: string;
	symbol: string;
	name: string;
	image: string;
	current_price: number;
	market_cap: number;
	market_cap_rank: number;
	fully_diluted_valuation?: number;
	total_volume: number;
	high_24h: number;
	low_24h: number;
	price_change_24h: number;
	price_change_percentage_24h: number;
	market_cap_change_24h: number;
	market_cap_change_percentage_24h: number;
	circulating_supply: number;
	total_supply?: number;
	max_supply?: number;
	ath: number;
	ath_change_percentage: number;
	ath_date: Date;
	atl: number;
	atl_change_percentage: number;
	atl_date: Date;
	last_updated: Date;
}

export const getMarketData = async () => {
	const params = {
		vs_currency: 'usd',
		ids: 'bitcoin,ethereum,dogecoin,avalanche,litecoin,bitcoin-cash,bitcoin,thorchain,binancecoin,usd-coin,tether,terrausd',
	};

	const marketData = await axios.get<MarketData[]>(
		'https://api.coingecko.com/api/v3/coins/markets',
		{
			params: params,
		},
	);

	return marketData.data;
};
